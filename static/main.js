var loginUrl = "/lpaInterface/app/getUserByUserName"
var getPlanBy5WeekUrl = "/lpaInterface/sendWeekAndMonth/getPlanBy5Week"
var getNoPlanUserUrl = "/lpaInterface/sendWeekAndMonth/getNoPlanUser"
var getOpenQuestionUrl = "/lpaInterface/sendWeekAndMonth/getOpenQuestion"
var getOverdueQuestionUrl = "/lpaInterface/sendWeekAndMonth/getOverdueIssue"
var getTextDisplayUrl = "/lpaInterface/sendWeekAndMonth/getTextDisplay"
var getPlanBy5WeekByMonthUrl = "/lpaInterface/sendWeekAndMonth/getPlanBy5WeekByMonth"
var getPlanByDivisionByMonthUrl = "/lpaInterface/sendWeekAndMonth/getPlanByDivisionByMonth"
var getPlanAndIssueByDivisionByMonthUrl = "/lpaInterface/sendWeekAndMonth/getPlanAndIssueByDivisionByMonth"
var getClassQuestionsByMonthUrl = "/lpaInterface/sendWeekAndMonth/getClassQuestionsByMonth"
var getOverIssueByMonthDataUrl = "/lpaInterface/sendWeekAndMonth/getOverIssueByMonth"
var getNoPlanByMonthAndDivisionUrl = "/lpaInterface/sendWeekAndMonth/getNoPlanByMonthAndDivision"
var getTextDisplayByMonthDataUrl =  "/lpaInterface/sendWeekAndMonth/getTextDisplayByMonth"
var getOverIssueByMonthListUrl =  "/lpaInterface/sendWeekAndMonth/getOverIssueByMonthList"
var getPlanIssueTOP5DataUrl="/lpaInterface/sendWeekAndMonth/getPlanIssueTOP5"
var getIuuseByDivisionBy3MonthUrl = "/lpaInterface/sendWeekAndMonth/getIuuseByDivisionBy3Month"
export function loginUser(e) {
	if (localStorage.getItem('userName')) {
		//模拟登录，将用户信息放到session里
		uni.request({
			url: loginUrl,
			data: {
				userAccount: localStorage.getItem('userName')
			},
			dataType: 'json',
			method: 'GET',
			success: (res) => {
				console.log(res)
				localStorage.setItem('user', res.data.user)
				localStorage.setItem('deptName', res.data.user.DEPARTMENT_NAME)
				
			},
			error(err) {},
		});
	} else {
		alert("缺少参数，请联系管理员")
	}
}

/**
 * 发送周报 -- 近五周审核完成率
 * @param response
 * @return
 * @throws Exception
 */
export function getPlanBy5Week() {
	return uni.request({
		url: getPlanBy5WeekUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}

/**
 * 发送周报 -- 未完成审核任务人员清单
 * @param response
 * @return
 * @throws Exception
 */
export function getNoPlanUserData() {
	return uni.request({
		url: getNoPlanUserUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送周报 -- 开放问题清单
 * @param response
 * @return
 * @throws Exception
 */
export function getOpenQuestionData() {
	return uni.request({
		url: getOpenQuestionUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送周报 -- 超期问题清单
 * @param response
 * @return
 * @throws Exception
 */
export function getOverdueQuestiuonData() {
	return uni.request({
		url: getOverdueQuestionUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送周报 -- 超期问题清单
 * @param response
 * @return
 * @throws Exception
 */
export function getTextDisplayData() {
	return uni.request({
		url: getTextDisplayUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送月报 -- 近五月审核完成率
 * @param response
 * @return
 * @throws Exception
 */
export function getPlanBy5WeekByMonthData() {
	return uni.request({
		url: getPlanBy5WeekByMonthUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}

/**
 * 发送月报 -- 当月各车间科室审核任务完成情况
 * @param response
 * @return
 * @throws Exception
 */
export function getPlanByDivisionByMonthData() {
	return uni.request({
		url: getPlanByDivisionByMonthUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送月报 -- 各科室审核问题提出情况
 * @param response
 * @return
 * @throws Exception
 */
export function getPlanAndIssueByDivisionByMonthData() {
	return uni.request({
		url: getPlanAndIssueByDivisionByMonthUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}
/**
 * 发送月报 -- 审核问题分类
 * @param response
 * @return
 * @throws Exception
 */
export function getClassQuestionsByMonthData() {
	return uni.request({
		url: getClassQuestionsByMonthUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}


/**
 * 发送月报 -- 超期问题各科室统计
 * @param response
 * @return
 * @throws Exception
 */

export function getOverIssueByMonthData() {
	return uni.request({
		url: getOverIssueByMonthDataUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},
		dataType: 'json',
		method: 'GET'
	});
}

/**
 * 发送月报 -- 未完成审核的各科室统计
 * @param response
 * @return
 * @throws Exception
 */

export function getNoPlanByMonthAndDivisionData() {
	return uni.request({
		url: getNoPlanByMonthAndDivisionUrl,
		data: {
			STARTTIME: localStorage.getItem('startTime'),
			ENDTIME: localStorage.getItem('endTime'),
			userName: localStorage.getItem("userName")
		},  
		dataType: 'json',
		method: 'GET'
	});
}

	/**
	 * 发送月报 -- 文字描述部分
		2025-2-5至 2025-2-9 XXX部，审核计划共73153项，完成67372项，计划完成率92.07%。
		四车间审核完成率最高，为98.43%，二车间审核计划完成率低于80%，需重点关注。
	 * @param response
	 * @return
	 * @throws Exception
	 */
	export function getTextDisplayByMonthData() {
		return uni.request({
			url: getTextDisplayByMonthDataUrl,
			data: {
				STARTTIME: localStorage.getItem('startTime'),
				ENDTIME: localStorage.getItem('endTime'),
				userName: localStorage.getItem("userName")
			},  
			dataType: 'json',
			method: 'GET'
		});
	}

	/**
	 * 发送月报 --  超期问题清单
	 * @param response
	 * @return
	 * @throws Exception
	 */
	export function getOverIssueByMonthListData() {
		return uni.request({
			url: getOverIssueByMonthListUrl,
			data: {
				STARTTIME: localStorage.getItem('startTime'),
				ENDTIME: localStorage.getItem('endTime'),
				userName: localStorage.getItem("userName")
			},  
			dataType: 'json',
			method: 'GET'
		});
	}
	/**
	 * 发送月报 --  超期问题清单
	 * @param response
	 * @return
	 * @throws Exception
	 */
	export function getPlanIssueTOP5Data() {
		return uni.request({
			url: getPlanIssueTOP5DataUrl,
			data: {
				STARTTIME: localStorage.getItem('startTime'),
				ENDTIME: localStorage.getItem('endTime'),
				userName: localStorage.getItem("userName")
			},  
			dataType: 'json',
			method: 'GET'
		});
	}
		
		/**
		 * 发送月报 -- 近3个月各车间/科室发起问题数量，人均提出数量
		 * @param response
		 * @return
		 * @throws Exception
		 */
	export function getIuuseByDivisionBy3MonthData() {
		return uni.request({
			url: getIuuseByDivisionBy3MonthUrl,
			data: {
				STARTTIME: localStorage.getItem('startTime'),
				ENDTIME: localStorage.getItem('endTime'),
				userName: localStorage.getItem("userName")
			},  
			dataType: 'json',
			method: 'GET'
		});
	}
